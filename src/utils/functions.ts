import { TLessonProps } from "../components/lesson/lesson.types"
import { TRowData } from "../components/row/row.types"

/**
 * Get min and max beginning and ending times from classes
 * 
 * @param {Array} classes Array of classes in format:
 */
export const classTimeRange = (days: TRowData[]) => {
    if (typeof days !== 'object') {
        console.error('Argument is not an array')
        return null
    }

    let limits = [Infinity, -Infinity]
    days.forEach((day) => {
        if (!day.subjects){
            return;
        }

        day.subjects.forEach((subject) => {
            if (subject.time.begin < limits[0]) {
                limits[0] = subject.time.begin
            }
            if (subject.time.end > limits[1]) {
                limits[1] = subject.time.end
            }
        })
    })

    return limits
}

/**
 * Get time difference in minutes
 * @param begin 
 * @param end 
 * @returns 
 */
export const timeDifference = (begin: number, end: number): number => {
    // if end is later than beginning, revert sign
    const sign = end > begin ? 1 : -1
    if (sign < 0) {
        // switch values and continue
        const tmp = end
        end = begin
        begin = tmp
    }
    // begin/end hours + minutes
    const ahr = Number(String(begin).slice(0, -2)),
        amin = Number(String(begin).slice(-2)),
        bhr = Number(String(end).slice(0, -2)),
        bmin = Number(String(end).slice(-2))

    if (bhr === ahr) {
        // diff in minutes
        return bmin - amin
    }

    // rest of minutes to end of hour plus difference of hours times 60min
    return ((60 - amin) + (bhr - (ahr + 1)) * 60 + bmin) * sign
}

/**
 * Get time in readable format 00:00
 * @param time 
 * @returns 
 */
export const timeReadableFormat = (time: number): string => {
    const timestr: string = String(time)
    // last two characters
    let m = timestr.slice(-2)
    // characters until the one before the last
    let h = timestr.slice(0, -2)

    return h + ':' + m
}

/**
 * Get array of overlapping subjects (subjects with the same time) and organize it into more lines
 * 
 * @param subjects List of subjects to sort
 * @returns Array of rows with subjects
 */
export const overlapRows = (subjects: TLessonProps[]): TLessonProps[][] => {
    // sort by beginning time from earliest to latest
    subjects = subjects.sort((st, nd) => {
        return st.time.begin - nd.time.begin
    })

    // array of overlapping subjects in rows
    const rows: TLessonProps[][] = [[]]

    for (const addedSubject of subjects) {        
        for (let row = 0; ;row++){
            if (!rows[row]){
                // if there is no row, create it
                rows[row] = []
            }
            // last subject in row, because it's sorted it's enough
            const comparedSubject = rows[row][rows[row].length - 1]

            if (!comparedSubject){
                // if there is no subject in row, add it
                rows[row].push(addedSubject)
                break;
            }
            // if subject is overlapping, increase row counter and continue. Because it's sorted, we don't need to check next subject
            // otherwise, add subject to row and break
            if (addedSubject.time.begin > comparedSubject.time.end){
                rows[row].push(addedSubject)
                break;
            }
        }
    }

    // just to make Typescript happy
    return rows
}