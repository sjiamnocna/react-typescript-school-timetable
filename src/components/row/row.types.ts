import { TLessonProps } from "../lesson/lesson.types"

export type TRowData = {
    title: string,
    note?: string,
    subjects?: TLessonProps[],
    className?: string,
    style?: React.CSSProperties
} 

//colWidth, className, subjects, title, note, beginTime
export type TRowProps = TRowData & {
    colWidth: number,
    beginTime: number,
}