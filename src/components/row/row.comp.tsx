import { overlapRows } from '../../utils/functions';
import ClassList from '../classList/classlist.comp';
import { TRowProps } from './row.types';

import './row.scss'

const TimetableRow = ({ colWidth, className, subjects, title, note, beginTime}: TRowProps) => {
    if (!subjects || subjects.length === 0){
        console.log('no subjects', subjects)
        return (
            <div className={`row${className ? ' ' + className : ''}`}>
                <div className='title'>
                    <p className='desc'>{title}</p>
                    {
                        note ? <p className='note'>{note}</p> : null
                    }
                </div>
                <div className='rows' />
            </div>
        )
    }
    
    let overlap = overlapRows(subjects)
    console.log('overlap', overlap, subjects)

    console.log(overlap ? 'overlap' : 'no overlap', subjects)

    return (
        <div className={`row${className ? ' ' + className : ''}`}>
            <div className='title'>
                <p className='desc'>{title}</p>
                {
                    note ? <p className='note'>{note}</p> : null
                }
            </div>
            <div className='rows'>
            {
                overlap.map((item, i) => {
                    console.log('classlists', item);
                    return <ClassList colWidth={colWidth} key={i} subjects={item} beginTime={beginTime} />
                })
            }            
            </div>
        </div>
    )
}

export default TimetableRow