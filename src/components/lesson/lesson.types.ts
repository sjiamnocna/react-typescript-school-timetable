export type TLessonProps = {
    shortcut: string
    title: string
    room: string
    teacher: string
    time: {
        begin: number
        end: number
    }
    className?: string
    style?: React.CSSProperties
}