import React from 'react'

import './lesson.scss'
import { TLessonProps } from './lesson.types'
import { timeReadableFormat } from '../../utils/functions'



/**
 * Lesson item
 * @param object props
 * @returns 
 */
const Lesson = ({ shortcut, title, room, teacher, time, className, style}: TLessonProps) => {
    return (
        <div className={`lesson-item${className ? ' ' + className : ''}`} style={style}>
            <div className="lesson-container">
                <time>
                    <span className="from">{timeReadableFormat(time.begin)}</span>
                    <span className="to">{timeReadableFormat(time.end)}</span>
                </time>
                <div className='lesson-data'>
                    <abbr title={title}>{shortcut}</abbr>
                    <p className="row teacher">{teacher}</p>
                    <p className="room">{room}</p>
                </div>
            </div>
        </div>
    )
}

export default Lesson