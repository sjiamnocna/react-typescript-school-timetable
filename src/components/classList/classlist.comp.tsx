import Lesson from '../lesson/lesson.comp'
import { TClassListProps } from './classlist.types'
import { timeDifference } from '../../utils/functions'

const ClassList = ({ colWidth, subjects, beginTime }: TClassListProps) => {
    return (
        <div className='content'>
            {
                subjects.map(((item, i) => {
                    const posLeft = timeDifference(beginTime, item.time.begin) * colWidth / 60
                    const width = Math.abs(timeDifference(item.time.end, item.time.begin) * colWidth / 60)

                    return (
                        // margin left relative to the previous class end
                        <Lesson
                            {...item}
                            style={{
                                // position left
                                'left': posLeft + "px",
                                'width': width + "px",
                            }}
                            key={i}
                        />
                    )
                }))
            }
        </div>
    )
}

export default ClassList