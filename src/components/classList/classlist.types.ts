import { TLessonProps } from "../lesson/lesson.types"

export type TClassListProps = {
    colWidth: number,
    subjects: TLessonProps[],
    beginTime: number,
}