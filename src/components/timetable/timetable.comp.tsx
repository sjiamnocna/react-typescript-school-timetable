import { classTimeRange } from '../../utils/functions';
import { TRowData } from '../row/row.types';
import { colWidth } from '../../utils/constants';
import TimetableRow from '../row/row.comp';

import './timetable.scss'

const Timetable = ({rows}: {rows: TRowData[]}) => {
  const limits = classTimeRange(rows)

  if (!limits){
    return null
  }

  
  return (<div className="timetable-container">
  {
    rows.map((day, i) => {
      const className = 'row' + (rows.length % 2 ? 'odd' : 'even') + (day.className ? ' ' + day.className : '')
      return (<TimetableRow key={i} className={className} colWidth={colWidth} beginTime={limits[0]} {...day} />)
    })
  } 
  </div>)
}

export default Timetable