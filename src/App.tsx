import Timetable from './components/timetable/timetable.comp'

function App() {
  return (
    <Timetable
      rows={[
        {
          title: '#',
          className: 'timetable-head',
        },
        {
          title: 'Po',
          className: 'timetable-day',
          subjects: [{
            title: 'Diskrétní Matematika',
            shortcut: 'KTE/DSM@',
            teacher: 'Doc. Pastor',
            room: 'ZI-N24',
            time: {
              begin: 800,
              end: 844
            },
            className: 'green',
          },{
            title: 'Diskrétní Matematika',
            shortcut: 'KTE/DSM@',
            teacher: 'Doc. Pastor',
            room: 'ZI-N24',
            time: {
              begin: 845,
              end: 1029
            },
            className: 'yellow',
          }],
        },
        {
          title: 'Út',
          className: 'timetable-day',
          subjects: [{
            title: 'Matematická Analýza 4',
            shortcut: 'KMT/MA4@',
            teacher: 'Doc. Laitochová',
            room: 'ZI-P4',
            time: {
              begin: 945,
              end: 1029
            },
            className: 'green',
          },{
            title: 'Matematická Analýza 4',
            shortcut: 'KMT/MA4@',
            teacher: 'Doc. Laitochová',
            room: 'ZI-P4',
            time: {
              begin: 1030,
              end: 1214
            },
            className: 'yellow',
          }, {
            title: 'Robotika v kurilulu sekundární školy',
            shortcut: 'KTE/RVK@',
            teacher: 'Mgr. Děrda',
            room: 'ZI-33',
            time: {
              begin: 1215,
              end: 1300
            },
            className: 'green',
          }, {
            title: 'Robotika v kurilulu sekundární školy',
            shortcut: 'KTE/RVK@',
            teacher: 'Mgr. Děrda',
            room: 'ZI-33',
            time: {
              begin: 1315,
              end: 1445
            },
            className: 'yellow',
          }, {
            title: 'Teorie systémů',
            shortcut: 'KTE/TS@',
            teacher: 'Prof. Klement',
            room: 'ZI-P4',
            time: {
              begin: 1315,
              end: 1445
            },
            className: 'green',
          }, {
            title: 'Teorie systémů',
            shortcut: 'KTE/TS@',
            teacher: 'Prof. Klement',
            room: 'ZI-P4',
            time: {
              begin: 1500,
              end: 1545
            },
            className: 'green',
          }],
        },
        {
          title: 'St',
          className: 'timetable-day',
          subjects: [{
            title: 'Geometrie 4',
            shortcut: 'KMT/GE4@',
            teacher: 'Mgr. Nocar',
            room: 'ZI-N24',
            time: {
              begin: 800,
              end: 844
            },
            className: 'green',
          },{
            title: 'Geometrie 4',
            shortcut: 'KMT/GE4@',
            teacher: 'Mgr. Nocar',
            room: 'ZI-N24',
            time: {
              begin: 845,
              end: 930
            },
            className: 'yellow',
          }, {
            title: 'Seminář z Geometrie',
            shortcut: 'KMT/SGE@',
            teacher: 'Mgr. Nocar',
            room: 'ZI-N24',
            time: {
              begin: 1215,
              end: 1359
            },
            className: 'yellow',
          }, {
            title: 'Algebra 4',
            shortcut: 'KMT/AL4@',
            teacher: 'Doc. Zdráhal',
            room: 'ZI-N24',
            time: {
              begin: 1500,
              end: 1544
            },
            className: 'green',
          },{
            title: 'Algebra 4',
            shortcut: 'KMT/AL4@',
            teacher: 'Doc. Zdráhal',
            room: 'ZI-N24',
            time: {
              begin: 1545,
              end: 1730
            },
            className: 'yellow',
          }],
        },
        {
          title: 'Čt',
          className: 'timetable-day',
          subjects: [{
            title: 'Seminář z Matalýzy',
            shortcut: 'KMT/SMA@',
            teacher: 'Doc. Laitochová',
            room: 'ZI-P4',
            time: {
              begin: 1030,
              end: 1214
            },
            className: 'green',
          }],
        },
      ]} />
  )
}

export default App