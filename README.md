# React school timetable component with typescript definitions
- Classic school timetable look
    - Days as rows
    - Subjects and hours as columns
- Handles overlapping classes

## Start
```bash
yarn && yarn dev
```

## Import component
`import Timetable from 'react-school-timetable'`